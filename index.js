
const lists=[
    {name: 'suger', price: 68, code:'cd1'},
    {name: 'rice', price: 85, code:'cd2' },
    {name: 'oil', price: 95, code:'cd3'},
    {name: 'pani-puri', price: 105, code:'cd4' },
    {name: 'pani-puri-masala', price: 45, code:'cd5' },
    {name: 'basmati-rice', price: 75, code:'cd6' },
    {name: 'chocolate', price: 185 , code:'cd7'},
    {name: 'dairy-milk', price: 285, code:'cd8' },
    {name: 'flower-vase', price: 385, code:'cd9' },
    {name: 'white-rabbit', price: 485, code:'cd10' },
    {name: 'sunflower-oil', price: 285, code:'cd11' },
    {name: 'til', price: 85, code:'cd12' },
    {name: 'puja-saman', price: 805, code:'cd13'},
    {name: 'dates', price: 850, code:'cd14'},
    {name: 'cashew', price: 905, code:'cd15' },
    {name: 'grains', price: 85, code:'cd16'},
    {name: 'rajma', price: 87, code:'cd17' }, 
    {name: 'lal-mohan', price: 88, code:'cd18' },
    {name: 'rasbari', price: 89, code:'cd19'},
    {name: 'orage', price: 90, code:'cd20' },
    {name: 'apple', price: 91, code:'cd21' },
    {name: 'juice', price: 92, code:'cd22' },
    {name: 'coke', price: 93, code:'cd23'},
    {name: 'fanta', price: 94, code:'cd24'},
    {name: 'sprite', price: 95, code:'cd25'},
    {name: 'beer', price: 96, code:'cd26' },
    {name: 'copy', price: 97 , code:'cd27'},
    {name: 'pen', price: 98, code:'cd28'},
    {name: 'vicks', price: 99, code:'cd29'},
    {name: 'almond', price: 100, code:'cd30' },
    {name: 'almond-oil', price: 101, code:'cd31' },
    {name: 'cream', price: 102 , code:'cd32'},
    {name: 'hair-oil', price: 103, code:'cd33' },
    {name: 'chat-masala', price: 104 , code:'cd34'},
    {name: 'kitkat', price: 105 , code:'cd35'},
    {name: 'bread', price: 106, code:'cd36' },

] 

function getProductObj(text){
    const search=lists.find(function(list){ 
    if(text===list.code)
        return list;
    });
    return search;
}  

function setNameAndPrice(nameId, priceId) { 
    let nameBox = document.getElementById(nameId);
    let productObj = getProductObj(nameBox.value);
    let priceBox = document.getElementById(priceId);
    nameBox.value = productObj.name;
    priceBox.value = productObj.price;
} 

function appendRow(){
    let rowCount = $('#billing_table > tbody > tr').length;
    let newRowId = `billing_table_row_${rowCount+1}_input_number`;
    let newInputNameId = `billing_table_row_${rowCount+1}_input_name`;
    let newInputPriceId = `billing_table_row_${rowCount+1}_input_price`;
    let newInputQuantityId = `billing_table_row_${rowCount+1}_input_quantity`;
    let newInputTotalId = `billing_table_row_${rowCount+1}_input_total`;
    let newRow=`
    <tr>
        <td><input value="${rowCount}" id="${newRowId}" disabled class="form-control form-control-md" type="text" placeholder=""></td>
        <td>
            <span><input id="${newInputNameId}" class="form-control form-control-md" type="text" placeholder="">
                        <button class="btn btn-info" type="button" 
                        onclick="setNameAndPrice('${newInputNameId}','${newInputPriceId}')">find</button>
            </span>
        </td>
        <td><input id="${newInputPriceId}" class="form-control form-control-md" type="number" placeholder=""></td>
        <td><input id="${newInputQuantityId}" class="form-control form-control-md" type="number" placeholder="">
            <span> 
                <button class="btn btn-info" type="button" 
                onclick="setTotal('${newInputQuantityId}','${newInputPriceId}','${newInputTotalId}')">total</button>
            </span>
        
        </td>
        <td><input id="${newInputTotalId}" class="form-control form-control-md" type="number" placeholder=""> </td>
        <td><button type="button" class="btn btn-danger" onclick="deleteRow(this)">-</button></td>
    </tr>`
    $("#billing_table_tbody").append(newRow);  
} 

function deleteRow(row){
    var tr = row.parentNode.parentNode; // Get the input's parent <tr>
    tr.parentNode.removeChild(tr); 
}

function calculateGrandTotal(){ 
    let sum=0; 
    let rowCount = $('#billing_table > tbody > tr').length;
    for(let i=2; i<=rowCount; i++){
        sum += +document.getElementById( `billing_table_row_${i}_input_total`).value;
    }
    document.getElementById('grandTotal').innerText = sum;
}

function setTotal(quantityInputId, priceInputId, totalInputId){
    // Get the input values
    a = Number(document.getElementById(quantityInputId).value);
    b = Number(document.getElementById(priceInputId).value);
    // Do the multiplication
    c = a*b;
    // Set the value of the total
    document.getElementById(totalInputId).value=c;
} 






  


   






